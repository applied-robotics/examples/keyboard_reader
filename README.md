
## Installing the Package

Clone this repository

```bash
$ git clone https://gitlab.cs.mcgill.ca/applied-robotics/examples/keyboard_reader
```

Build the repository 
```bash
$ catkin build keyboard_reader
$ source devel/setup.bash
```

## Running the Node

```bash
# In one terminal, run
$ roscore

# In another terminal, run
$ rosrun keyboard_reader keyboard_reader
```



## Usage

```
Reading from the keyboard  and Publishing to topic teleop/cmd

CTRL-C to quit
```
------


#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/String.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <map>


// Reminder message
const char* msg = R"(

Reading from the keyboard and Publishing to teleop_msgs!
---------------------------
Moving around:
   u    i    o
   
anything else : stop

CTRL-C to quit

)";

// For non-blocking keyboard inputs
int getch(void) {

	int ch;
	struct termios oldt;
	struct termios newt;

	// Store old settings, and copy to new settings
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;

	// Make required changes and apply the settings
	newt.c_lflag &= ~(ICANON | ECHO);
	newt.c_iflag |= IGNBRK;
	newt.c_iflag &= ~(INLCR | ICRNL | IXON | IXOFF);
	newt.c_lflag &= ~(ICANON | ECHO | ECHOK | ECHOE | ECHONL | ISIG | IEXTEN);
	newt.c_cc[VMIN] = 1;
	newt.c_cc[VTIME] = 0;
	tcsetattr(fileno(stdin), TCSANOW, &newt);

	// Get the current character
	ch = getchar();

	// Reapply old settings
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

	return ch;
}

int main(int argc, char** argv) {

	// Init ROS node
	ros::init(argc, argv, "keyboard_reader");
	ros::NodeHandle nh;

	// Init cmd_vel publisher
	ros::Publisher teleop_publisher = nh.advertise<std_msgs::String>("/keyboard_reader/cmd", 1);
	std_msgs::String teleop_msgs ;
	char key(' ');

	while(true){
		// Get the pressed key
		key = getch();
		teleop_msgs.data = key ;
		// If ctrl-C (^C) was pressed, terminate the program
		if (key == '\x03') {
			printf("\n\n                 .     .\n              .  |\\-^-/|  .    \n             /| } O.=.O { |\\\n\n                 CH3EERS\n\n");
			break;
		}
		teleop_publisher.publish(teleop_msgs);
		ros::spinOnce();
	}

	return 0;
}
